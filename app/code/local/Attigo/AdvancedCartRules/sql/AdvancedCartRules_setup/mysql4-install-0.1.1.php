<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
$installer = $this;

$installer->startSetup();

$tableName = $installer->getTable('salesrule/rule');

$query = 'SHOW COLUMNS FROM ' . $tableName;

$tableColumns = $installer->getConnection()->fetchCol($query);

if (!in_array('products_sku', $tableColumns)) {

    $installer->getConnection()->addColumn(
            $installer->getTable('salesrule/rule'), //table name
            'products_sku', //column name
            'TEXT'  //datatype definition
    );

    $installer->getConnection()->addColumn($installer->getTable('sales/quote_item'), 'is_free_product', "tinyint(4) NOT NULL default '0'");
    $installer->getConnection()->addColumn($installer->getTable('sales/order_item'), 'is_free_product', "tinyint(4) NOT NULL default '0'");
}

$installer->endSetup();
