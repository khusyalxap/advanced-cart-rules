<?php

/**
 * AdvancedCartRules helper
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * This logs a message in a custom log file.
     * 
     * @param string $msg
     */
    public function log($msg) {
        Mage::log($msg, Zend_Log::INFO, 'AdvancedCartRules.log');
    }

    /**
     * Prepare lable for products_sku field
     *
     * @return string
     */
    public function getProductsSkuLabel() {
        return Mage::helper('AdvancedCartRules')->__('Promotional Items SKUs');
    }

    /**
     * Prepare comment for products_sku field
     *
     * @return string
     */
    public function getProductsSkuComment() {
        return Mage::helper('AdvancedCartRules')->__('Comma separated list of the products SKUs');
    }
    /**
     * add drop down under salesrule action
     *	parameter : Array
     * @return Array
     */
    public function addOptions($formAction) {
		/* Add two dropdown of our module */
        if ($formAction) {
            $selectionVals = $formAction->getValues();
            $selectionVals[] = array(
                'value' => 'promotional_item',
                'label' => Mage::getStoreConfig('AdvancedCartRules/general/promotional_item'),
            );
            $selectionVals[] = array(
                'value' => 'promotional_cart',
                'label' => Mage::getStoreConfig('AdvancedCartRules/general/promotional_cart'),
            );
        }
        return $selectionVals;
    }

    /**
     * prefix value of free item
     *
     * @return string
     */
    public function getPrefixLable() {
        return Mage::getStoreConfig('AdvancedCartRules/general/freeitemlabel');
    }

    /**
     * Custom Message with product
     *
     * @return string
     */
    public function getCustomMessage() {
        return Mage::getStoreConfig('AdvancedCartRules/general/custom_message');
    }

    /**
     * Free Product delete enable/disable
     *
     * @return string
     */
    public function isDeleteAllow() {
        return Mage::getStoreConfig('AdvancedCartRules/general/deletefreeitem');
    }

    /**
     * Show Message on cart page when promotional product is deleted
     *
     * @return 
     */
    public function displayMessage($error = true, $visibility=false) {
		/* get message if user deleted few item from mcart */
        $message = $this->_DeleteItemMessage();
		
		/* get session object */
        $session = Mage::getSingleton('checkout/session');
		
		/* convert message in to string */
        $allMessage = $session->getMessages(false)->toString();
        if (false !== strpos($allMessage, $message))
            return;
		/* if debuger get an error */
        if ($error && isset($_GET['debug'])) {
            $session->addError($message);
        } else {
            $messageArray = $session->getPromotionalMessages();
			/* no message set previously */
            if (!is_array($messageArray)) {
                $messageArray = array();
            }
			/* make message for user visiability */
            if (!in_array($message, $messageArray) || $visibility) {
                $session->addNotice($message);
                $messageArray[] = $message;
                $session->setPromotionalMessages($messageArray);
            }
        }
    }

    /**
     * Show link with message  For restoring Promotional Products
     *
     * @return string
     */
    public function _DeleteItemMessage() {
        return Mage::helper('AdvancedCartRules')->__(
                        '<a href="%s">Restore your free products</a>', Mage::getUrl('AdvancedCartRules/index/index')
        );
    }
    
   
    
    /**
     * Message if item is outof stock  
     *
     * @return string
     */
    public function outofStockMessage($appliedFreeQty, $sku) {      
        $session = Mage::getSingleton('checkout/session');
        $session->setoutofStockMessage($session->addNotice($this->__('Free Item with Quantity %d Of  SKU `%s` is not in the stock', $appliedFreeQty, $sku)));
    }
	/**
	 * Add Notice message on cart page
	*/
	public function displayNoticeMessage($noticeMessage, $error = true, $visibility=false) {
		/* Set Notice Message  */
        $message = $noticeMessage;
		
		/* get session object */
        $session = Mage::getSingleton('checkout/session');
		
		/* convert message in to string */
        $allMessage = $session->getMessages(false)->toString();
        if (false !== strpos($allMessage, $message))
            return;
		/* if debuger get an error */
        if ($error && isset($_GET['debug'])) {
            $session->addError($message);
        } else {
            $messageArray = $session->getPromotionalMessages();
			/* no message set previously */
            if (!is_array($messageArray)) {
                $messageArray = array();
            }
			/* make message for user visiability */
            if (!in_array($message, $messageArray) || $visibility) {
                $session->addNotice($message);
                $messageArray[] = $message;
                $session->setPromotionalMessages($messageArray);
            }
        }
    }

}