<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Block_Adminhtml_Promo_Quote_Edit extends Mage_Adminhtml_Block_Promo_Quote_Edit {

    public function __construct() {
        parent::__construct();
        /* function would be call onchange under action dropdown on shopping cart rule */
        if (Mage::getStoreConfig('AdvancedCartRules/general/enable')):
            $this->_formScripts[] = " 
			function showhidefun() {
				$('rule_discount_qty').up().up().show();
					$('rule_discount_step').up().up().show();
					$('rule_apply_to_shipping').up().up().show();
					$('rule_actions_fieldset').up().show();
					$('rule_products_sku').up().up().hide();
				if ('".Attigo_AdvancedCartRules_Model_Consts::ADD_PROMOTIONALCART_ACTION."' == $('rule_simple_action').value) {
				    $('rule_actions_fieldset').up().hide(); 
					$('rule_discount_qty').up().up().hide();
					$('rule_discount_step').up().up().hide();
					$('rule_apply_to_shipping').up().up().hide();
					$('rule_products_sku').up().up().show();
				} 
				if ('".Attigo_AdvancedCartRules_Model_Consts::ADD_PROMOTIONAL_ACTION."' == $('rule_simple_action').value){
					$('rule_apply_to_shipping').up().up().hide();
					$('rule_products_sku').up().up().show();
				}
				
			}
			showhidefun();
        ";
        endif;
    }

}