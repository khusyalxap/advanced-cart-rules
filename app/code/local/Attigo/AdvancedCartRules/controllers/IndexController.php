<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_IndexController extends Mage_Core_Controller_Front_Action 
{
    /*
     *For Restoring Deleted free promotional item in cart
     */
    public function indexAction() 
	{
		/* Empty session variable if item allready in the cart */
        Mage::getSingleton('checkout/session')->setPromotionalDeletedItems(null);
		/* Empty message if free items added in the cart */
        Mage::getSingleton('checkout/session')->setPromotionalMessages(null);
		/* Redirecting backto cart page */
        $this->_redirect('checkout/cart/index');
    }
}