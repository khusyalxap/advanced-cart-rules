<?php
/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Model_Observer {
    // for avoiding multiple addition of free product
    protected $_isRuleApplied = array();
    /**
     * Delete all free products that have been added through this module before.
     * This is done before discounts are given in on the event 
     * 'sales_quote_collect_totals_before'.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteCollectTotalsBefore(Varien_Event_Observer $observer) {
		if(!Mage::getStoreConfig('AdvancedCartRules/general/enable')){//Check if extension enabled
		return;
		}
        self::_resetFreeItems($observer->getEvent()->getQuote());
    }
    /**
     * Update cart 
     * 
     * @param Varien_Event_Observer $observer
     */
    public function checkoutCartUpdateItemsBefore($observer) {
		/* get info of the observer */
        $instanceInfo = $observer->getInfo();
		/* get whole quote for active session */
        $quote = $observer->getCart()->getQuote();
		/* make sure only free item can be removed */
        foreach (array_keys($instanceInfo) as $itemId) {
			/* get item from quote */
            $item = $quote->getItemById($itemId);
			/* if item not exist in the quote */
            if (!$item)
                continue;
			/* if current item is a promotional(free) item */	
            if (!$item->getIsFreeProduct())
                continue;
			/* if info not exist for current item */	
            if (empty($instanceInfo[$itemId]))
                continue;
			/* make free item removal */	
            $instanceInfo[$itemId]['remove'] = true;
        }
        return $this;
    }

    /**
     * Add Free Item to the cart, if the current salesrule is of simple action 
     * ADD_PROMOTIONAL_ACTION. The rule has been validated before the event
     * 'salesrule_validator_process' is thrown that we catch.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function salesruleValidatorProcess(Varien_Event_Observer $observer) {
		
		if(!Mage::getStoreConfig('AdvancedCartRules/general/enable')){//Check if extension enabled
		return;
		}
        /* @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();
        /* @var $item Mage_Sales_Model_Quote_Item */
        $item = $observer->getEvent()->getItem();
        /* @var $rule Mage_SalesRule_Model_Rule */
        $rule = $observer->getEvent()->getRule();

        /* check is it a promotional item */
        if ($item->getIsFreeProduct()):
            return $this;
        endif;
        /* check if promotional item all ready added to cart */
        if (isset($this->_isRuleApplied[$rule->getId()])):
            return $this;
        endif;
        /* set promotional rule is applied  */
        $this->_isRuleApplied[$rule->getId()] = true;

        /* if rule is fired by our promotional module add add free item to cart */
        if ($rule->getSimpleAction() == Attigo_AdvancedCartRules_Model_Consts::ADD_PROMOTIONAL_ACTION || $rule->getSimpleAction() == Attigo_AdvancedCartRules_Model_Consts::ADD_PROMOTIONALCART_ACTION):
            // if rule applied on our module and action are own module add free product with quote
            self::_handleFreeItems($quote, $item, $rule);
			
        endif;
    }

    /**
     * Make sure that a free product is only added once, create a free item and add it to the cart.
     * 
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Sales_Model_Quote_Item $item
     * @param Mage_Sales_Model_Quote_Item $rule
     */
    protected static function _handleFreeItems(Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Quote_Item $item, Mage_SalesRule_Model_Rule $rule) {
        $session = Mage::getSingleton('checkout/session');
        // check if its a new quote or existiong if new reset session variable
        if ($session->getPromotionalId() != $quote->getId()):
		/* empty session variable free item allready in the cart */
            $session->setPromotionalDeletedItems(null);
		/* empty message session variable free item are in the cart */	
            $session->setPromotionalMessages(null);
		/* set current quote in the session variable */	
            $session->setPromotionalId($quote->getId());
        endif;
        // if rule is not enable 
        if ($rule->getIsApplied()):
            return;
        endif;
        $productsSkus = $rule->getProductsSku();
        // if rule promotional sku field is empty
        if (!$productsSkus):
            return;
        endif;
        // how many quantity is applied with the rule
        $appliedFreeQty = (integer) Attigo_AdvancedCartRules_Model_Rendrer::appliedFreeQty($rule, $quote);
		/* how much quantity applicable for free product */
        if ($appliedFreeQty) :
            foreach (explode(',', $productsSkus) as $promotional_product_sku):
                $promotional_product_sku = trim($promotional_product_sku);
                // if  free product sku not available with the rule
                if (!$promotional_product_sku):
                    continue;
                endif;
				/* get each free product in the rule by sku */
                $freeItem = self::_getFreeQuoteItem($quote, $promotional_product_sku, $item->getStoreId(), $appliedFreeQty);
                // If free is available     
                if ($freeItem):
				/* if free item exist add it in the quote */
                    self::_addAndApply($quote, $freeItem, $rule);
					// Show Rule Label on cart as notice on rule applied
					$lavelMessage = $rule->getStoreLabel(Mage::app()->getStore());
					if ($lavelMessage){
						Mage::helper('AdvancedCartRules')->displayNoticeMessage($lavelMessage, false);	
					}
                endif;
            endforeach;
        endif;
    }

    /**
     * Add a free item and mark that the rule was used on this item.
     * 
     * @param Mage_Sales_Model_Quote $quote
     * @param Mage_Sales_Model_Quote_Item $item
     * @param Mage_Sales_Model_Quote_Item $rule
     */
    protected static function _addAndApply(Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Quote_Item $item, Mage_SalesRule_Model_Rule $rule) {
		
 		/* get message you want to show with product */
		$messageWithFreeProduct = Mage::helper('AdvancedCartRules')->getCustomMessage();
		if ($messageWithFreeProduct){
			/* set message with free item */
			$item->setMessage($messageWithFreeProduct);
		}
				
		/* Add free item with the quote */
        $quote->addItem($item);
		/* apply cart rule on free product */
        $item->setApplyingRule($rule);
		/* make sure rule has been applied */
        $rule->setIsApplied(true);
    }

    /**
     * Create a free item. It has a value of 0$ in the cart no matter what the price was
     * originally.
     * 
     * @param Mage_Sales_Model_Quote $quote
     * @param string $sku
     * @param int $storeId
     * @param int $qty
     * @return Mage_Sales_Quote_Item
     */
    protected static function _getFreeQuoteItem(Mage_Sales_Model_Quote $quote, $sku, $storeId, $appliedFreeQty) {
		/* if applied quantity is not set */
        if ($appliedFreeQty < 1) {
            return;
        }
		
		/* get session object */
        $session = Mage::getSingleton('checkout/session');
		/* get free item those are recently deleted */
        $PromotionalDeletedItems = Mage::getSingleton('checkout/session')->getPromotionalDeletedItems();
        // if promotional item not deleted reset the array
        if (!is_array($PromotionalDeletedItems)):
            $PromotionalDeletedItems = array();
        endif;
        // if promotional item deleted set message for restoring it in cart
        if (isset($PromotionalDeletedItems[$sku])):
            if (Mage::app()->getRequest()->getControllerName() == 'cart'):
		/* display message only on cart page with the help of controller */	
                Mage::helper('AdvancedCartRules')->displayMessage(false, true);
            endif;
            return false;
        endif;
		/* load free product by sku attribute */
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        // If product does'nt load
        if (!$product):
            return false;
        endif;
		/* for checking product availability in the stock */
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        // If required qty is not available
        if ($stock->getQty() < $appliedFreeQty || !$stock->getIsInStock()) :
		/* if quy is not available and product is outof stock display the message */
            Mage::helper('AdvancedCartRules')->outofStockMessage($appliedFreeQty, $sku);
            return false;
        else:
            // make blank session variable if free item is in stock
            $session->setoutofStockMessage();
        endif;
		if ('multishipping' === Mage::app()->getRequest()->getControllerName()){
                return false;
            }
		/* if the initial  quote was virtual */	
		 if (!$product->isVirtual()){
                $quote->getBillingAddress()->setTotalAmount('subtotal', 0);
            }
				
        Mage::getModel('cataloginventory/stock_item')->assignProduct($product);
		
		/* get free product prefix from config*/
		$getPrefix = Mage::helper('AdvancedCartRules')->getPrefixLable();
		
		/* add prefix before product name */
		$product->setName($getPrefix.' '.$product->getName());
		
        $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
		/* set free item with quote by setting zero price */
         $quoteItem->setQuote($quote)
                ->setQty($appliedFreeQty)
				->setName($quoteItem->getName())
                ->setCustomPrice(0.0)
                ->setOriginalCustomPrice($product->getPrice())
                ->setIsFreeProduct(true)
                ->setStoreId($storeId);
        return $quoteItem;
    }

    /**
     * Delete free products that have been added through this module before.
     * This is done validation process 
     * 'sales_quote_remove_item'.
     * 
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteRemoveItem(Varien_Event_Observer $observer) {
		/* get deleted item */
        $item = $observer->getEvent()->getQuoteItem();
        $model = Mage::getModel('AdvancedCartRules/Abstract');
		/* delete free product from cart */
        return $model->deleteFreeProduct($item);
    }

    /**
     * Reset free items from the cart.
     * 
     * @param Mage_Sales_Model_Quote $quote
     */
    protected function _resetFreeItems(Mage_Sales_Model_Quote $quote) {
        /* initiallize the rule */
        $this->_isRuleApplied = array();
        $model = Mage::getModel('AdvancedCartRules/Abstract');
		/* initialize free product with quote */
        return $model->freeitemInitialize($quote);
    }

}
