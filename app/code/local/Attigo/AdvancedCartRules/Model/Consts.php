<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Model_Consts {
	/* two action available for module */
	
	/* action will handle if rule applied on item */
    const ADD_PROMOTIONAL_ACTION = 'promotional_item';
	
	/* action will handle if rule applied on whole cart */
    const ADD_PROMOTIONALCART_ACTION = 'promotional_cart';
}