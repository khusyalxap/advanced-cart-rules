<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Model_Config {

    /**
     * Set data at the time of cartrule creation.
     *
     * @public
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     * @return Object
     */
    public function formHandler($observer) {
		/* if module is not enable retuen */
        if (!Mage::getStoreConfig('AdvancedCartRules/general/enable')):
            return;
        endif;
		
		/* get form which hold the simple action */ 
        $formAction = $observer->getForm()->getElement('simple_action');

		/* get dropdown associate with our module and existing values  */
        $selectionVals = Mage::helper('AdvancedCartRules')->addOptions($formAction);
		
		/* set dropdown values with from action */
        $formAction->setValues($selectionVals);
		
		/* hide our custome field if rule is  not associate with our module using js */
        $formAction->setOnchange('showhidefun()'); // js function is defined in class Attigo_AdvancedCartRules_Block_Adminhtml_Promo_Quote_Edit

		/* get existing field */
        $fieldSet = $observer->getForm()->getElement('action_fieldset');
		
		/* add an extra field as text field which will hold the products skus comma seprated */
        $fieldSet->addField('products_sku', 'text', array('name' => 'products_sku',
            'label' => Mage::helper('AdvancedCartRules')->getProductsSkuLabel(),
            'note' => Mage::helper('AdvancedCartRules')->getProductsSkuComment(),
                ), 'discount_amount'
        );

        return $this;
    }

}
