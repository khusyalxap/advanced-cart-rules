<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Model_Abstract {
	 /**
     * Initialize  free items.
     * 
     * @param Mage_Sales_Model_Quote $quote
     */
   public function freeitemInitialize(Mage_Sales_Model_Quote $quote){
	   /* get session object */
  		$session = Mage::getSingleton('checkout/session');
		/* if no item is added in the cart */
	   if (!$quote) 
            return $this;
		/* trace all items in tha cart */
        foreach ($quote->getItemsCollection() as $item) {
	    /* take the action only on free item */
            if (!$item->getIsFreeProduct()) {
                continue;
            }
		/* if item not exist in the quote */	
            if (!$item) {
                continue;
            }
		/* empty session variable */	
			$session->setPromotionalItem();
		/* add free item id in session variable for managing in feture like deletion action */	
			$session->setPromotionalItem($item->getId());
		/* initialize with single quantity */	
            $item->setData('qty_to_add', '0.0');
            $item->isDeleted(true);
		/* remove item if allready in the cart */	
            $quote->removeItem($item->getId());
            
        }
        return $this;
   }
    /**
     * Delete free products that have been added through this module before.
     *  
     *  
     * 
     * @param Mage_Sales_Model_Quote_Item $item
     */
    public function deleteFreeProduct(Mage_Sales_Model_Quote_Item $item){
		/* get session object */
  		$session = Mage::getSingleton('checkout/session');
		/* if free item is not deleted before */
	   if ($item->getId() != $session->getPromotionalItem()) {
		/* check if free item deletion is allow */   
            $deletefreeitem = Mage::helper('AdvancedCartRules')->isDeleteAllow();
		/* if deletion allow */	
            if ($deletefreeitem) {
		/* get delete items array those are deleted beforev */		
                $deletedItems = $session->getPromotionalDeletedItems();
		/* if no item deleted before reset the array */		
                if (!is_array($deletedItems)) {
                    $deletedItems = array();
                }
		/* add currently deleted item in array */		
                $deletedItems[$item->getSku()] = true;
		/* assign deleted array to session */		
                $session->setPromotionalDeletedItems($deletedItems);
		/* for managing quote in feture */		
                $session->setPromotionalId($item->getQuote()->getId());
            }
        }
   }

}
