<?php

/**
 * AdvancedCartRules config model
 *
 * @category    Salesrule
 * @package     Attigo_AdvancedCartRules
 * @author      Attigo <>
 */
class Attigo_AdvancedCartRules_Model_Rendrer {

    /**
     * Set data at the time of cartrule creation.
     *
     * @public
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     * @return Object
     */
    public function appliedFreeQty(Mage_SalesRule_Model_Rule $rule, Mage_Sales_Model_Quote $quote) {
        /* Get Maximum Discount amount for the rule */
        $_discountAmount = max(1, $rule->getDiscountAmount());
        $qty = 0;
        /* If rule is applied on whole cart */
        if (Attigo_AdvancedCartRules_Model_Consts::ADD_PROMOTIONALCART_ACTION == $rule->getSimpleAction()) {
            $qty = $_discountAmount;
        } else {
            /* if rule applied on cart items */
            $discountStep = max(1, $rule->getDiscountStep());
			/* trace quote whole items and apply rule on free product */
            foreach ($quote->getItemsCollection() as $item) {
			/* if no item exist */	
                if (!$item)
                    continue;
			/* if item belongs to free product skip */		
                if ($item->getIsFreeProduct())
                    continue;
			/* if rule is not validate continue */		
                if (!$rule->getActions()->validate($item)) {
                    continue;
                }
			/* combine the quantity */	
                $qty = $qty + $item->getQty();
            }
			/* set quantity if user add enough quantity in the cart for getting free product*/
            $qty = floor($qty / $discountStep) * $_discountAmount;
			/* get maximum discount quantity set in the cart rule */
            $max = $rule->getDiscountQty();
            if ($max) {
			/* get applicable value for quantity */	
                $qty = min($max, $qty);
            }
        }
        return $qty;
    }

}
